package com.abbtech.apigw.controller;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(name = "userFeignClient", url = "http://localhost:8088/abbtech-bookstore-user-ms")
public interface UserFeignClient {

    @GetMapping("/internal/auth")
    ResponseEntity<Void> auth(@RequestParam(name = "path") String path, @RequestParam(name = "method") HttpMethod method,@RequestParam(name = "token") String token);

}
