package com.abbtech.apigw.dto.request;

import java.util.List;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

public class UserRegisterRequestDto {
    @NotBlank
    private String username;
    @NotBlank
    private String password;
    @NotEmpty
    private List<@NotBlank String> roles;
}
