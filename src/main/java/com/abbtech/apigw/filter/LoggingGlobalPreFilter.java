package com.abbtech.apigw.filter;

//@Component
//@RequiredArgsConstructor
//public class LoggingGlobalPreFilter implements Filter {
//
//    private static final Logger logger = LoggerFactory.getLogger(LoggingGlobalPreFilter.class);
//    private final UserFeignClient userFeignClient;
//
//
//
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
//        HttpServletRequest httpRequest = (HttpServletRequest) request;
//        HttpServletResponse httpResponse = (HttpServletResponse) response;
//        String path = httpRequest.getRequestURI();
//        System.out.println(path);
//        HttpMethod requestMethod = HttpMethod.valueOf(httpRequest.getMethod());
//
//        if (path.startsWith("/api/user/auth")) {
//            logger.info("Logging user auth");
//            filterChain.doFilter(request, response);
//        } else {
//
//            String authHeader = httpRequest.getHeader("Authorization");
//            String token = null;
//            if (authHeader != null && authHeader.startsWith("Bearer ")) {
//                token = authHeader.substring(7);
//            }
//            ResponseEntity<Void> auth = userFeignClient.auth(path, requestMethod,token);
//
//            if (auth.getStatusCode().is2xxSuccessful()) {
//                System.out.println("ok");
//                filterChain.doFilter(request, response);
//            } else {
//                httpResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
//                httpResponse.getWriter().write("Not authenticated");
//                httpResponse.getWriter().flush();
//            }
//        }
//
//    }
//
//}

import java.io.IOException;
import com.abbtech.apigw.controller.UserFeignClient;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@RequiredArgsConstructor
public class LoggingGlobalPreFilter extends OncePerRequestFilter {

    private final UserFeignClient userFeignClient;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String path = request.getRequestURI();
        HttpMethod httpMethod = HttpMethod.valueOf(request.getMethod());
        System.out.println(path);
        CustomHttpServletRequestWrapper muttableRequest = new CustomHttpServletRequestWrapper(request);



        if (path.startsWith("/api/user/auth")) {
            logger.info("Logging user auth");
            filterChain.doFilter(request, response);
        } else {

            String authHeader = request.getHeader("Authorization");
            String token = null;
            if (authHeader != null && authHeader.startsWith("Bearer ")) {
                token = authHeader.substring(7);
            }
            ResponseEntity<Void> auth = userFeignClient.auth(path, httpMethod,token);

            if (auth.getStatusCode().is2xxSuccessful()) {
                System.out.println("ok");
                System.out.println(auth.getHeaders().getFirst("X-USER-ID"));

                muttableRequest.addHeader("X-USER-ID", auth.getHeaders().getFirst("X-USER-ID"));
                muttableRequest.addHeader("X-USER-EMAIL",auth.getHeaders().getFirst("X-USER-EMAIL"));
                filterChain.doFilter(muttableRequest, response);
            } else {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.getWriter().flush();
            }
        }
    }

}